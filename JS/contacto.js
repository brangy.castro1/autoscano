function enviarConcto(){
    var nombre = $("#nombre").val();
    var email = $("#emial").val();
    var telefono = $("#telefono").val();
    var mensaje = $("#mensaje").val();

    db.collection("contacto").add({
        "nombre": nombre,
		"email": email,
        "telefeno": telefono,
        "mensaje": mensaje
    })
    .then(function(docRef) {
        limpiarContacto();
        toastr.success("Infromacion de contacto enviada","Exito");
    })
    .catch(function(error) {
        toastr.error("No se a podido Guardar","Aviso!");
    });

}

function limpiarContacto(){
    $("#nombre").val("");
    $("#emial").val("");
    $("#telefono").val("");
    $("#mensaje").val("");
}

function validarGuardadoContacto(){
    var nombre = $("#nombre").val();
    var email = $("#emial").val();
    var telefono = $("#telefono").val();
    var mensaje = $("#mensaje").val();

    if($.trim(nombre) == ""){
        toastr.error("Ingrese su Nombre","Aviso!");
        $("#nombre").addClass("error");
    }else if($.trim(email) == ""){
        $("#nombre").removeClass("error");
        toastr.error("Ingrese su Correo","Aviso!");
        $("#emial").addClass("error");
    }else if($.trim(telefono) == ""){
        $("#emial").removeClass("error");
        toastr.error("Ingrese su Telefono","Aviso!");
        $("#telefono").addClass("error");
    }else if($.trim(mensaje) == ""){
        $("#telefono").removeClass("error");
        toastr.error("Ingrese su Mensaje","Aviso!");
        $("#mensaje").addClass("error");
    }else{
        $("#mensaje").removeClass("error");
        enviarConcto();
    }
}

function validateMail(idMail){
	// Patron para el correo
	var patron=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if(!idMail.search(patron)==0){
        //Mail incorrecto
        toastr.error("Correo Incorecto","Aviso!");
        $("#emial").addClass("error");
	}
}

function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57)
}