
function enCarga(){
    listarVehiculo();
    filtrarMarcasVehiculo();
    filtrarModeloVehiculo();
    ocultarBoton();
    $("#divBuscar").hide('slow');
    $("#datos").hide('slow');
    $("#divMarcas").hide('slow');
    $("#divModelos").hide('slow');
    $("#mostrar").hide('slow');
}

function guardarVehiculo(){

    matricula = $('#matricula').val();
	marca = $('#selectMarcaVehiculo').val();
	modelo = $('#selectModelo').val();
	tipo = $('#tipo').val();

    trans = document.getElementsByName('transmision');
    valor = null;
    if(trans[0].checked){
        valor = trans[0].value;
    }
    if(trans[1].checked){
        valor = trans[1].value;
    }
    if(valor == null){
        alert("selecciona la transmision");
        return
    }
    transmision = $('#transmision').val();
    color = $('#color').val();
    motor = $('#motor').val();
    chasis = $('#chasis').val();
    tarifa = $('#tarifa').val();
    
    db.collection("vehiculo").add({
        "matricula": matricula,
		"marca": marca,
		"modelo": modelo,
		"tipo": tipo,
		"transmision": valor,
		"color": color,
		"motor": motor,
		"chasis": chasis,
		"tarifa": tarifa
    })
    .then(function(docRef) {
        cancelarVehiculo();
        toastr.success("Datos Guardados","Exito!");
    })
    .catch(function(error) {
        toastr.warning("Datos no guardados","Aviso!");
    });
    
}

function borrarVehiculo(id){
    var respuesta = confirm("Seguro que desea Eliminar el Vehiculo");
    if (respuesta == true) {
        db.collection("vehiculo").doc(id).delete().then(function() {
            toastr.warning("Datos eliminado con éxito!","Aviso!");
        }).catch(function(error) {
            toastr.error("Problema al elminar los datos","Aviso!");
        });
    }else{
        toastr.warning("Datos no Eliminados","Aviso!");
    }
}

function actualizarVehiculo(id, matricula, marca, modelo, tipo, transmision, motor, chasis, color, tarifa){
    $("#contenedorFormulario").show('slow');
    $("#divBoton").show('slow');
    $("#datos").hide('slow');
    $("#divBuscar").hide('slow');
    desbloquearCampos();
    mostrarBoton();

    alert(transmision);

    if(transmision == "AT"){
        $("#transmisionAT").prop('checked', true);
    }else{
        $("#transmisionME").prop('checked', true);
    }

    $("#selectMarcaVehiculo").prop('disabled', true);
    $("#selectModelo").prop('disabled', true);

    $('#matricula').val(matricula);
	$('#selectMarcaVehiculo').val(marca);
	$('#selectModelo').val(modelo);
	$('#tipo').val(tipo);
    $('#color').val(color);
    $('#motor').val(motor);
    $('#chasis').val(chasis);
    $('#tarifa').val(tarifa);

    var btnModificar = document.getElementById('btnGuardarVehiculo');
    btnModificar.innerHTML = 'Modificar';
    btnModificar.onclick = function() {
        var respuesta = confirm("Seguro que desea modificar los datos del Vehiculo: " + marca + " " + modelo);
        if (respuesta == true) {

        matricula = $('#matricula').val();
        marca = $('#selectMarcaVehiculo').val();
        modelo = $('#selectModelo').val();
        tipo = $('#tipo').val();

        trans = document.getElementsByName('transmision');
        valor = null;
        if(trans[0].checked){
            valor = trans[0].value;
        }
        if(trans[1].checked){
            valor = trans[1].value;
        }
        if(valor == null){
            alert("selecciona la transmision");
            return
        }
        transmision = $('#transmision').val();
        color = $('#color').val();
        motor = $('#motor').val();
        chasis = $('#chasis').val();
        tarifa = $('#tarifa').val();

        var clienteRef = db.collection("vehiculo").doc(id);
            return clienteRef.update({
                "matricula": matricula,
                "marca": marca,
                "modelo": modelo,
                "tipo": tipo,
                "transmision": valor,
                "color": color,
                "motor": motor,
                "chasis": chasis,
                "tarifa": tarifa
            })
            .then(function() {
                toastr.success("Datos actualidados","Exito!");
                cancelarVehiculo();
            })
            .catch(function(error) {
                toastr.warning("Error de la base de datos","Aviso!");
                cancelarVehiculo();
            });
          }else{
            toastr.warning("Datos no actualidados","Aviso!");
            cancelarVehiculo();
          } 

        }
}

function listarVehiculo(){
    var destino = document.getElementById('tbDestino');
    db.collection("vehiculo").onSnapshot((querySnapshot) => {
        destino.innerHTML = '';
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <tr>
                <td>${doc.data().matricula}</td>
                <td>${doc.data().marca}</td>
                <td>${doc.data().modelo}</td>
                <td>${doc.data().tipo}</td>
                <td>${doc.data().transmision}</td>
                <td>${doc.data().color}</td>
                <td>${doc.data().motor}</td>
                <td>${doc.data().chasis}</td>
                <td>${doc.data().tarifa}</td>
                <td><button class="btn btn-danger" onclick="borrarVehiculo('${doc.id}');"><i class="fas fa-trash"></i></button></td>
                <td><button class="btn btn-warning" 
                    onclick="actualizarVehiculo(
                        '${doc.id}','${doc.data().matricula}',
                        '${doc.data().marca}','${doc.data().modelo}',
                        '${doc.data().tipo}','${doc.data().transmision}',
                        '${doc.data().motor}','${doc.data().chasis}',
                        '${doc.data().color}','${doc.data().tarifa}'
                        )">
                        <i class="fas fa-edit"></i>
                    </button>
                </td>
            </tr>
            `
        });
    });
}

function editMarcas(){
    destino = document.getElementById('divListaMarcas');
    db.collection("marca").onSnapshot((querySnapshot) => {
        destino.innerHTML = '';
        querySnapshot.forEach((doc) => {
            div = document.createElement('div');
            div.setAttribute('class','form-group input-group');
            span = document.createElement('span');
            span.setAttribute('class','input-group-addon');
            span.innerHTML = 'x';
            span.setAttribute('ondblclick','this.parentNode.parentNode.removeChild(this.parentNode);');
            div.appendChild(span);
            txt = document.createElement('input');
            txt.setAttribute('class','form-control');
            txt.setAttribute('type','text');
            txt.setAttribute('name','listMarcas');
            txt.value = doc.data().nombre;
            div.appendChild(txt);
            document.getElementById('divListaMarcas').appendChild(div);
        });
    });

    $("#divMarcas").show('slow');
    $("#divMain").hide('slow');
    $("#divBtn").hide('slow');
    $("#divModelos").hide('slow');
    $("#tabla").hide('show');
    $("#btnGuardarMarca").hide('show');
}

function agregarMarcas() {
    $("#btnGuardarMarca").show('show');
    $("#btnMasMarca").hide('show');
    $("#btnRegresarMarca").hide('show');
    $("#txtMarca").prop('disabled', false);

    var cancelar = document.getElementById('btnCancelar');
    cancelar.style.visibility = "visible";
    cancelar.onclick = function(){
        cancelar.style.visibility = "hidden";
        $("#btnMasMarca").show('show');
         $("#btnRegresarMarca").show('show');
         $("#btnGuardarMarca").hide('show');
         $("#txtMarca").prop('disabled', true);
    }
}

function guardarMarcas(){
   var marca = $("#txtMarca").val();

   db.collection("marca").add({
    nombre: marca
})
.then(function(docRef) {
    $("#txtMarca").val("");
    $("#btnMasMarca").show('show');
    $("#btnRegresarMarca").show('show');
    $("#btnGuardarMarca").hide('show');
    $("#btnCancelar").hide('show');
    $("#txtMarca").prop('disabled', true);
    console.log("Document written with ID: ", docRef.id);
})
.catch(function(error) {
    console.error("Error adding document: ", error);
});
}

function editModelo(){
    db.collection("modelo").onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            listarMarcaModelo(doc.data().idMarca, doc.data().nombre)
        });
    });
    filtrarMarcas();
    $("#divModelos").show('slow');
    $("#divMain").hide('slow');
    $("#divMarcas").hide('slow');
    $("#tabla").hide('show');
    $("#divBtn").hide('slow');
    $("#btnGuardarModelo").hide('show');
}

function listarMarcaModelo(idMarca, nombre){
    destino = document.getElementById('divListaModelos');
    destino.innerHTML = '';
    db.collection("marca").doc(idMarca)
    .onSnapshot(function(doc) {
        destino.innerHTML += `
            <tr>
                <td>${doc.data().nombre}</td>
                <td>${nombre}</td>
            </tr>        `
    });
}

function filtrarMarcasVehiculo(){
    var destino = document.getElementById('selectMarcaVehiculo');
    db.collection("marca").onSnapshot((querySnapshot) => {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <option value="${doc.data().nombre}" id="${doc.id}">${doc.data().nombre}</option>
            `
        });
    });
}

function filtrarMarcas(){
    var destino = document.getElementById('selectMarca');
    db.collection("marca").onSnapshot((querySnapshot) => {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <option value="${doc.id}">${doc.data().nombre}</option>
            `
        });
    });
}

function filtrarModelo(idMarca){
    var marca = idMarca[idMarca.selectedIndex].id
    destino = document.getElementById('selectModelo');
    db.collection("modelo").where("idMarca", "==", marca)
    .onSnapshot(function(querySnapshot) {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach(function(doc) {
            destino.innerHTML += `
                 <option value="${doc.data().nombre}" id="${doc.id}">${doc.data().nombre}</option>    
            `
        });
    });
}

function filtrarModeloVehiculo(){
    var destino = document.getElementById('selectModelo');
    db.collection("modelo").onSnapshot((querySnapshot) => {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <option value="${doc.data().nombre}" id="${doc.id}">${doc.data().nombre}</option>
            `
        });
    });
}

function agregarModelo(){
    $("#btnGuardarModelo").show('show');
    $("#btnMasModelo").hide('show');
    $("#btnRegresarModelo").hide('show');
    $("#txtModelo").prop('disabled', false);
    $("#selectMarca").prop('disabled', false);

    var cancelar = document.getElementById('btnCancelarModelo');
    cancelar.style.visibility = "visible";
    cancelar.onclick = function(){
        cancelar.style.visibility = "hidden";
        $("#btnMasModelo").show('show');
        $("#btnRegresarModelo").show('show');
        $("#btnGuardarModelo").hide('show');
        $("#txtModelo").prop('disabled', true);
        $("#selectMarca").prop('disabled', true);
    }
}

function guardarModelo(){
    var marca = $("#selectMarca").val();
    var modelo = $("#txtModelo").val();

    db.collection("modelo").add({
        idMarca: marca,
        nombre: modelo
    })
    .then(function(docRef) {
        $("#txtModelo").val("");
        $("#btnMasModelo").show('show');
        $("#btnRegresarModelo").show('show');
        $("#btnGuardarModelo").hide('show');
        $("#btnCancelarModelo").hide('show');
        $("#txtModelo").prop('disabled', true);
        $("#selectMarca").prop('disabled', true);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
}

function irAlInicio() {
    $("#divMarcas").hide('slow');
    $("#divModelos").hide('slow');
    $("#divMain").show('slow');
    $("#tabla").show('show');
    $("#divBtn").show('slow');
}

function limpiarVehiculo(){
    $('#matricula').val("");
	$('#selectMarcaVehiculo').val("");
	$('#selectModelo').val("");
    $('#tipo').val("");
    $("#transmisionAT").prop('checked', false);
    $("#transmisionME").prop('checked', false);
    $('#color').val("");
    $('#motor').val("");
    $('#chasis').val("");
    $('#tarifa').val("");
}

function desbloquearCampos(){
    $("#matricula").prop('disabled', false);
    $("#selectMarcaVehiculo").prop('disabled', false);
    $("#selectModelo").prop('disabled', false);
    $("#tipo").prop('disabled', false);
    $("#transmisionAT").prop('disabled', false);
    $("#transmisionME").prop('disabled', false);
    $("#color").prop('disabled', false);
    $("#motor").prop('disabled', false);
    $("#chasis").prop('disabled', false);
    $("#tarifa").prop('disabled', false);
}

function bloquearCampos(){
    $("#matricula").prop('disabled', true);
    $("#selectMarcaVehiculo").prop('disabled', true);
    $("#selectModelo").prop('disabled', true);
    $("#tipo").prop('disabled', true);
    $("#transmisionAT").prop('disabled', true);
    $("#transmisionME").prop('disabled', true);
    $("#color").prop('disabled', true);
    $("#motor").prop('disabled', true);
    $("#chasis").prop('disabled', true);
    $("#tarifa").prop('disabled', true);
}

function nuevoVehiculo(){
    desbloquearCampos();
    mostrarBoton();
}

function cancelarVehiculo(){
    bloquearCampos();
    limpiarVehiculo();
    ocultarBoton();
    $("#botonMarMol").show('slow');
}

function mostrarBoton(){
    $("#btnCancelarVehiculo").show('slow');
    $("#btnGuardarVehiculo").show('slow');
    $("#btnNuevoVehiculo").hide('slow');
    $("#btnBuscarVehiculo").hide('slow');
}

function ocultarBoton(){
    $("#btnCancelarVehiculo").hide('slow');
    $("#btnGuardarVehiculo").hide('slow');
    $("#btnNuevoVehiculo").show('slow');
    $("#btnBuscarVehiculo").show('slow');
}

function buscarVehiculo(){
    $("#botonMarMol").hide('slow');
    $("#divBoton").hide('slow');
    $("#divBuscar").show('slow');
    $("#contenedorFormulario").hide('slow');
    $("#datos").show('slow');
}

function ocultarBuscarVehiculo(){
    ocultarBoton();
    $("#divBuscar").hide('slow');
    $("#datos").hide('slow');
    $("#contenedorFormulario").show('slow');
    $("#botonMarMol").show('slow');
}

function buscarGeneralVehiculo(){
    var tableReg = document.getElementById('datos');
    var searchText = document.getElementById('searchTerm').value.toLowerCase();
    var cellsOfRow="";
    var found=false;
    var compareWith="";

    // Recorremos todas las filas con contenido de la tabla
    for (var i = 1; i < tableReg.rows.length; i++)
    {
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        found = false;
        // Recorremos todas las celdas
        for (var j = 0; j < cellsOfRow.length && !found; j++)
        {
            compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            // Buscamos el texto en el contenido de la celda
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
            {
                found = true;
            }
        }
        if(found)
        {
            tableReg.rows[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg.rows[i].style.display = 'none';
        }
    }
}

function validarPlaca(){
    var valorPlaca = document.getElementById("matricula").value;
    var digitos = valorPlaca.length;
  
    // Aqui esta el patron(expresion regular) a buscar en el input
    patronPlaca = /^[a-zA-Z]{3}([-])[0-9]{4}$/;
  
    if( patronPlaca.test(valorPlaca) ){
      $("#matricula").removeClass("error");
    }
    else{
        toastr.error("Porfavor ingrese una Placa validad","Aviso!");
      $("#matricula").addClass("error");
    }
}

function validarAutoExistente(matricula){
    console.log(matricula);
    db.collection("vehiculo").where("matricula", "==", matricula)
    .onSnapshot(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            toastr.error("Matricula ya existe..!!","Aviso!");
            $("#matricula").addClass("error");
            $("#matricula").val("");
        });
    });
}

function validarGuardado(){
    matricula = $('#matricula').val();
	marca = $('#selectMarcaVehiculo').val();
	modelo = $('#selectModelo').val();
	tipo = $('#tipo').val();

    trans = document.getElementsByName('transmision');
    valor = null;
    if(trans[0].checked){
        valor = trans[0].value;
    }
    if(trans[1].checked){
        valor = trans[1].value;
    }
    color = $('#color').val();
    motor = $('#motor').val();
    chasis = $('#chasis').val();
    tarifa = $('#tarifa').val();

    if(matricula == ""){
        $("#matricula").addClass("error");
        toastr.error("Ingrese Matricula","Aviso!");
    }else if(marca == ""){
        $("#matricula").removeClass("error");
        toastr.error("Seleccione Marca","Aviso!");
        $("#selectMarcaVehiculo").addClass("error");
    }else if(modelo == ""){
        $("#selectMarcaVehiculo").removeClass("error");
        toastr.error("Seleccione Modelo","Aviso!");
        $("#selectModelo").addClass("error");
    }else if(tipo == ""){
        $("#selectModelo").removeClass("error");
        toastr.error("Ingrese Tipo","Aviso!");
        $("#tipo").addClass("error");
    }else if(valor == ""){
        $("#tipo").removeClass("error");
        toastr.error("selecciona la transmision","Aviso!");
        $("#transmision").addClass("error");
    }else if(motor == ""){
        $("#transmision").removeClass("error");
        toastr.error("Ingrese No. Motor","Aviso!");
        $("#motor").addClass("error");
    }else if(chasis == ""){
        $("#motor").removeClass("error");
        toastr.error("Ingrese No. Chasis","Aviso!");
        $("#chasis").addClass("error");
    }else if(color == ""){
        $("#chasis").removeClass("error");
        toastr.error("Ingrese Color","Aviso!");
        $("#color").addClass("error");
    }else if(tarifa == ""){
        $("#color").removeClass("error");
        toastr.error("Ingrese Color","Aviso!");
        $("#tarifa").addClass("error");
    }else{
        $("#tarifa").removeClass("error");
        guardarVehiculo();
    }
}