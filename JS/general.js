
// *******************************************************
// ********************* BASE DE DATOS *************************
// *******************************************************

firebase.initializeApp({
    apiKey: "AIzaSyDgmMopQEYphN-yj4dPJIdJxaUIjbqozC4",
    authDomain: "autoscano-c6ce9.firebaseapp.com",
    databaseURL: "https://autoscano-c6ce9.firebaseio.com",
    projectId: "autoscano-c6ce9",
    storageBucket: "autoscano-c6ce9.appspot.com",
    messagingSenderId: "255190501253"
  });
  var db = firebase.firestore();


// *******************************************************
// ********************* GENERAL *************************
// *******************************************************

var datos = {
		vehiculo: [],
		marcas: [],
		alquiler: [],
		modelos: [],
		cliente: [],
};

var veh_en_edicion = null;


function guardarTodo(){
	localStorage.setItem('asgData',JSON.stringify(datos));
}

// *******************************************************
// ******************** VEHICULO *************************
// *******************************************************

// ********************************************************************************
// Metodo para guardar vehiculos en un arreglo
// ********************************************************************************
	function guardarVehiculo(){
		matricula = $('#matricula').val();
		marca = $('#marca').val();
		modelo = $('#modelo').val();
		tipo = $('#tipo').val();

		trans = document.getElementsByName('transmision');
		valor = null;
		if(trans[0].checked){
			valor = trans[0].value;
		}
		if(trans[1].checked){
			valor = trans[1].value;
		}
		if(valor == null){
			alert("selecciona la transmision");
			return
		}
		transmision = $('#transmision').val();
		color = $('#color').val();
		motor = $('#motor').val();
		chasis = $('#chasis').val();
		tarifa = $('#tarifa').val();


			  
			db.collection("vehiculo").add({
				"matricula": matricula,
				"marca": marca,
				"modelo": modelo,
				"tipo": tipo,
				"transmision": valor,
				"color": color,
				"motor": motor,
				"chasis": chasis,
			  	"tarifa": tarifa
			})
			.then(function(docRef) {
				console.log("Document written with ID: ", docRef.id);
			})
			.catch(function(error) {
				console.error("Error adding document: ", error);
			});

			if(veh_en_edicion == null){
				datos.vehiculo.push(veh);
				nuevoVehiculo();
			}else{
				datos.vehiculo[veh_en_edicion] = veh;
				veh_en_edicion = null;
				nuevoVehiculo();

			}

		mostrarVehiculos();

	}

// ********************************************************************************
// Metodo para mostrar los vehiculo en una tabla
// ********************************************************************************
	function mostrarVehiculos(){
		var destino = document.getElementById('tbDestino');
		db.collection("vehiculo").onSnapshot((querySnapshot) => {
			destino.innerHTML = '';
			querySnapshot.forEach((doc) => {
				console.log(`${doc.id} => ${doc.data()}`);
				destino.innerHTML = `
				<tr>
					<td>${doc.data().matricula}</td>
					<td>${doc.data().marca}</td>
					<td>${doc.data().modelo}</td>
					<td>${doc.data().tipo}</td>
					<td>${doc.data().transmision}</td>
					<td>${doc.data().color}</td>
					<td>${doc.data().motor}</td>
					<td>${doc.data().chasis}</td>
					<td>${doc.data().tarifa}</td>
				</tr>
				`
			});
		});
	}
// ********************************************************************************
// Metodo q nos permite cagar en un select lo que tenemos almacedano 
// en el localStore en este caso son las MARCAS de los vehiculos
// ********************************************************************************

	function cargarSelect(){
		marca = document.getElementById('marca');
		marca.innerHTML = "";
		for(x=0; x < datos.marcas.length; x++){
			opt = document.createElement('option');
			opt.text = datos.marcas[x];
			opt.value = datos.marcas[x];
			marca.appendChild(opt);
		}
		marca.selectedIndex = -1;
	}
// ********************************************************************************
// Metodo que nos sirve para cargar todo los datos que
// se encuentran en el localStorage
// ********************************************************************************
	function cargarTodoV(){
		tmp = localStorage.getItem('asgData');

		if(tmp != null){
			datos = JSON.parse(tmp);
		}
		cargarSelect();
		mostrarVehiculos();
	}
// ********************************************************************************
// Metodo para eliminar los vehiculos ingresado en el localStorage
// ********************************************************************************
	function borrarVeh(index){
		if(confirm("Seguro que quiere eliminar el vehiculo")){
			datos.vehiculo.splice(index, 1);
			guardarTodo();
			mostrarVehiculos();
		}
	}
// ********************************************************************************
// Metodo para editar los vehiculos ingresados en el localStorage
// ********************************************************************************
	function editVeh(index){
		veh_en_edicion = index;
		veh = datos.vehiculo[index];
		for(key in veh){
			$('#'+key).val(veh[key]);
		}
		filtrarModelos(veh.marca);
		$("#modelo").val(veh.modelo);

		document.getElementById('transmision'+veh.transmision).checked = true;
	}
// ********************************************************************************
// Metodo que nos permite editar la MARCAS 
// ********************************************************************************
	function editMarcas(){
		destino = document.getElementById('divListaMarcas');
		destino.innerHTML = "";
		for(x=0; x < datos.marcas.length; x++){
			agregarMarcas(datos.marcas[x]);
		}
		$("#divMarcas").show('slow');
		$("#divMain").hide('slow');
		$("#divModelos").hide('slow');
	}
// ********************************************************************************
// Metodo que nos permite editar los MODELOS
// ********************************************************************************
	function editModelo(){
		destino = document.getElementById('divListaModelos');
		destino.innerHTML = "";
		for(y=0; y < datos.modelos.length; y++){
			agregarModelos(datos.modelos[y]);
			}

		$("#divModelos").show('slow');
		$("#divMain").hide('slow');
		$("#divMarcas").hide('slow');
		}
// ********************************************************************************
// Metodo que nos permite irnos a la pantall de ingresar 
// vehiculos
// ********************************************************************************
	function irAlInicio() {
		$("#divMarcas").hide('slow');
		$("#divModelos").hide('slow');
		$("#divMain").show('slow');
	}
// ********************************************************************************
// Metodo para agregar marcas 
// ********************************************************************************
	function agregarMarcas(val) {
		div = document.createElement('div');
		div.setAttribute('class','form-group input-group');
		span = document.createElement('span');
		span.setAttribute('class','input-group-addon');
		span.innerHTML = 'x';
		span.setAttribute('ondblclick','this.parentNode.parentNode.removeChild(this.parentNode);');
		div.appendChild(span);
		txt = document.createElement('input');
		txt.setAttribute('class','form-control');
		txt.setAttribute('type','text');
		txt.setAttribute('name','listMarcas');
		txt.value = val;
		div.appendChild(txt);
		document.getElementById('divListaMarcas').appendChild(div);

	}
// ********************************************************************************
// Metodo para guardar las marcas
// ********************************************************************************
	function guardarMarcas() {
		marcas = document.getElementsByName('listMarcas');
		datos.marcas = [];
		for(x=0; x < marcas.length; x++){
			datos.marcas.push(marcas[x].value);
		}
		guardarTodo();
		cargarSelect();
	}
// ********************************************************************************
// Metodo para editar los modelos
// ********************************************************************************
	function editModelo(){
		destino = document.getElementById('divListaModelos');
		destino.innerHTML = "";
		for(y=0; y < datos.modelos.length; y++){
			agregarModelos(datos.modelos[y]);
		}
		$("#divModelos").show('slow');
		$("#divMain").hide('slow');
		$("#divMarcas").hide('slow');
	}

	function dce(e){
		return document.createElement(e);
	}
// ********************************************************************************
// Metodo para agregar modelos 
// ********************************************************************************
	function agregarModelos(arr_modelo){
		destino = document.getElementById('divListaModelos');

		tr = dce('tr');
		td = dce('td');
		sel = dce('select');

		for(z=0; z < datos.marcas.length; z++){
			opt = dce('option');
			opt.text = datos.marcas[z];
			opt.value = datos.marcas[z];
			sel.appendChild(opt);
		}
		$(sel).val(arr_modelo[0]);
		sel.setAttribute('class','form-control');
		sel.setAttribute('name','mod-marca');
		td.appendChild(sel);
		tr.appendChild(td);


		td = dce('td');
		txt = dce('input');
		txt.setAttribute('name','mod-name');
		txt.setAttribute('class','form-control');
		txt.value = arr_modelo[1];
		td.appendChild(txt);
		tr.appendChild(td);

		destino.appendChild(tr);
	}
// ********************************************************************************
// Metodo para guardar las modelos
// ********************************************************************************
	function guardarModelos() {
		marcas = document.getElementsByName('mod-marca');
		modelos = document.getElementsByName('mod-name');
		datos.modelos = [];
		for(x=0; x < modelos.length; x++){
			datos.modelos.push([marcas[x].value, modelos[x].value]);
		}
		guardarTodo();
		cargarSelect();
	}
// ********************************************************************************
// Metodo para filtrar los modelos en los select
// ********************************************************************************
	function filtrarModelos(p_marca) {
		destino = document.getElementById('modelo');
		destino.innerHTML = "";

		for(x=0; x < datos.modelos.length; x++){
			mod = datos.modelos[x];
				if(mod[0] == p_marca){
						opt = dce('option');
						opt.text = mod[1];
						opt.value = mod[1];

						destino.appendChild(opt);
				}
		}
		 destino.selectedIndex = -1;
	}
// ********************************************************************************
// Metodo para limpiar y referscar la ventana
// ********************************************************************************
	function nuevoVehiculo(){
			window.location = window.location;
	}

	function buscarVehiculo() {
		$("#divMain").hide("slow");
		$("#mostrar").show("slow");
	}

	function regresarVehiculo(){
		$("#divMain").show("slow");
		$("#mostrar").hide("slow");
	}
	// *******************************************************
	// ******************** CLIENTE **************************
	// *******************************************************

 var cli_en_edicion = null;
// ********************************************************************************
// Metodo para guardar las cliente en un arreglo
// ********************************************************************************
	function guardarCliente(){
	  cedula = $('#cedula').val();
	  nombres = $('#nombres').val();
	  apellidos = $('#apellidos').val();
	  ciudad = $('#ciudad').val();
	  direccion = $('#direccion').val();
	  telefono1 = $('#telefono1').val();
	  telefono2 = $('#telefono2').val();
	  genero = $('#genero').val();
	  fecha = $('#fecha').val();
	  correo = $('#correo').val();

	db.collection("cliente").add({
		cedula: cedula,
	    nombres: nombres,
	    apellidos: apellidos,
	    ciudad: ciudad,
	    direccion: direccion,
	    telefono1: telefono1,
	    telefono2: telefono2,
	    genero: genero,
	    fecha: fecha,
	    correo: correo
	})
	.then(function(docRef) {
		console.log("Document written with ID: ", docRef.id);
	})
	.catch(function(error) {
		console.error("Error adding document: ", error);
	});

			// if(cli_en_edicion == null){
			// 	datos.cliente.push(cli);
			// 	nuevoVehiculo();
			// }else{
			// 	datos.cliente[cli_en_edicion] = cli;
			// 	cli_en_edicion = null;
			// 	nuevoCliente();
			// }

	//  guardarTodo();
	// 	mostrarClientes();
		
		

	}
// ********************************************************************************
// Metodo para mostrar los cliente en una tabla
// ********************************************************************************
	function mostrarClientes(){
	  destino = document.getElementById('tbDestino');
	  destino.innerHTML = "";
	  for(x=0; x < datos.cliente.length; x++){
	    cli = datos.cliente[x];
	    tr = document.createElement('tr');
	    for(key in cli){
	      prop = cli[key];
	      td = document.createElement('td');
	      td.innerHTML = prop;
	      tr.appendChild(td);
	    }
	    td = document.createElement('td');
	    btn = document.createElement('button');
	    btn.type='button';
	    btn.setAttribute('class','btn btn-danger');
	    btn.innerHTML = "Eliminar";
	    btn.setAttribute('onClick','borrarCli('+x+');');
	    td.appendChild(btn);

	    btn = document.createElement('button');
	    btn.type='button';
	    btn.setAttribute('class','btn btn-warning');
	    btn.innerHTML = "Editar";
	    btn.setAttribute('onClick','editCli('+x+');');
	    td.appendChild(btn);

	    tr.appendChild(td);
	    destino.appendChild(tr);
	  }
	}
// ********************************************************************************
// Metodo para cargar todos los elementos guardados 
// en el localStorage
// ********************************************************************************
	function cargarTodoC(){
	  tmp = localStorage.getItem('asgData');

	  if(tmp != null){
	    datos = JSON.parse(tmp);
	  }
	  mostrarClientes();
	}
// ********************************************************************************
// Metodo para guardar las marcas
// ********************************************************************************
	function nuevoCliente() {
		window.location = window.location;
	}
// ********************************************************************************
// Metodo para borrar un ciente
// ********************************************************************************
	function borrarCli(index){
		if(confirm("Seguro que quiere eliminar el cliente")){
			datos.cliente.splice(index, 1);
			guardarTodo();
			mostrarClientes();
		}
	}
// ********************************************************************************
// Metodo para editar un cliente
// ********************************************************************************
	function editCli(index){
		cli_en_edicion = index;
		cli = datos.cliente[index];
		for(key in cli){
			$('#'+key).val(cli[key]);
		}
		$("#genero").val(cli.cliente);
		calcularEdad();
	}

	function buscar() {
		$("#ocultarCliente").hide("slow");
		$("#mostrar").show("slow");
	}

	function regresarCliente(){
		$("#ocultarCliente").show("slow");
		$("#mostrar").hide("slow");
	}
// ********************************************************************************
// Metodo para guardar los cliente, vehiculos y 
// los alquileres en la tabla
// ********************************************************************************
	function buscarGeneral(){

			var tableReg = document.getElementById('datos');
			var searchText = document.getElementById('searchTerm').value.toLowerCase();
			var cellsOfRow="";
			var found=false;
			var compareWith="";

			// Recorremos todas las filas con contenido de la tabla
			for (var i = 1; i < tableReg.rows.length; i++)
			{
				cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
				found = false;
				// Recorremos todas las celdas
				for (var j = 0; j < cellsOfRow.length && !found; j++)
				{
					compareWith = cellsOfRow[j].innerHTML.toLowerCase();
					// Buscamos el texto en el contenido de la celda
					if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
					{
						found = true;
					}
				}
				if(found)
				{
					tableReg.rows[i].style.display = '';
				} else {
					// si no ha encontrado ninguna coincidencia, esconde la
					// fila de la tabla
					tableReg.rows[i].style.display = 'none';
				}
			}
}


	// *******************************************************
	// ******************** ALQUILER **************************
	// *******************************************************

var alq_en_edicion = null;
// ********************************************************************************
// Metodo para cargar todos los alquileres
// ********************************************************************************
function cargarTodoAl(){
	tmp = localStorage.getItem('asgData');

	if(tmp != null){
		datos = JSON.parse(tmp);
	}
	cargarSelectCl();
	cargarSelectMa();
	mostrarAlquiler();
}
// ********************************************************************************
// Metodo para cargar los cliente en un select
// ********************************************************************************
function cargarSelectCl(){
	cedulaAl = document.getElementById('cedulaAl');
	cedulaAl.innerHTML = "";
	for(x=0; x < datos.cliente.length; x++){
		opt = document.createElement('option');
		opt.text = datos.cliente[x].cedula;
		opt.value = datos.cliente[x].cedula;
		cedulaAl.appendChild(opt);
	}
	cedulaAl.selectedIndex = -1;
}
// ********************************************************************************
// Metodo para cargar los vehiculos en un select
// ********************************************************************************
function cargarSelectMa(){
	matriculaAl = document.getElementById('matriculaAl');
	matriculaAl.innerHTML = "";
	for(x=0; x < datos.vehiculo.length; x++){
		opt = document.createElement('option');
		opt.text = datos.vehiculo[x].matricula;
		opt.value = datos.vehiculo[x].matricula;
		matriculaAl.appendChild(opt);
	}
	matriculaAl.selectedIndex = -1;
}
// ********************************************************************************
// Metodo para rellenar los campos auotmaticamente
// una vez ingresado el numero de cedula
// ********************************************************************************
function mostrarClientesAl(c_cedula){

	for(x=0; x < datos.cliente.length; x++){
			if(datos.cliente[x].cedula == c_cedula){
				$('#nombresAl').val(datos.cliente[x].nombres);
				$('#apellidosAl').val(datos.cliente[x].apellidos);
				$('#direccionAl').val(datos.cliente[x].direccion);
			}
	}
}
// ********************************************************************************
// Metodo para rellenar los campos auotmaticamente
// una vez ingresado la matricula
// ********************************************************************************
function mostrarVehiculoAl(v_Matricula){

	for(x=0; x < datos.vehiculo.length; x++){
			if(datos.vehiculo[x].matricula == v_Matricula){
				$('#marcaAl').val(datos.vehiculo[x].marca);
				$('#modeloAl').val(datos.vehiculo[x].modelo);
				$('#tarifaAl').val(datos.vehiculo[x].tarifa);
			}
	}
}
// ********************************************************************************
// Metodo para guardar un alquiler en un arreglo
// ********************************************************************************

function guardarAlquiler(){
	servicio = $('#servicio').val();
	cedulaAl = $('#cedulaAl').val();
	nombresAl = $('#nombresAl').val();
	apellidosAl = $('#apellidosAl').val();
	direccionAl = $('#direccionAl').val();
	matriculaAl = $('#matriculaAl').val();
	marcaAl = $('#marcaAl').val();
	modeloAl = $('#modeloAl').val();
	tarifaAl = $('#tarifaAl').val();
	salida = $('#salida').val();
	entrada = $('#entrada').val();
	observacion = $('#observacion').val();
	estado = $('#estado').val();
	pagar = $('#pagar').val();

	alq = {
		"servicio": servicio,
		"cedulaAl": cedulaAl,
		"nombresAl": nombresAl,
		"apellidosAl": apellidosAl,
		"direccionAl": direccionAl,
		"matriculaAl": matriculaAl,
		"marcaAl": marcaAl,
		"modeloAl": modeloAl,
		"tarifaAl": tarifaAl,
		"salida": salida,
		"entrada": entrada,
		"observacion": observacion,
		"estado": estado,
		"pagar": pagar}

		if(alq_en_edicion == null){
			datos.alquiler.push(alq);
			nuevoAlquiler();
		}else{
			datos.alquiler[alq_en_edicion] = alq;
			alq_en_edicion = null;
			nuevoAlquiler();
		}

		guardarTodo();
		mostrarAlquiler();
}
// ********************************************************************************
// Metodo para mostrar los alquileres en una tabla
// ********************************************************************************

function mostrarAlquiler(){
	destino = document.getElementById('tbDestino');
	destino.innerHTML = "";
	for(x=0; x < datos.alquiler.length; x++){
		cli = datos.alquiler[x];
		tr = document.createElement('tr');
		for(key in cli){
			prop = cli[key];
			td = document.createElement('td');
			td.innerHTML = prop;
			tr.appendChild(td);
		}
		td = document.createElement('td');
		btn = document.createElement('button');
		btn.type='button';
		btn.setAttribute('class','btn btn-danger');
		btn.innerHTML = "Eliminar";
		btn.setAttribute('onClick','borrarAlq('+x+');');
		td.appendChild(btn);

		btn = document.createElement('button');
		btn.type='button';
		btn.setAttribute('class','btn btn-warning');
		btn.innerHTML = "Editar";
		btn.setAttribute('onClick','editAlq('+x+');');
		td.appendChild(btn);

		tr.appendChild(td);
		destino.appendChild(tr);
	}
}
// ********************************************************************************
// Metodo para eliminar un alquiler
// ********************************************************************************

function borrarAlq(index){
	if(confirm("Seguro que quiere eliminar el alquiler")){
		datos.alquiler.splice(index, 1);
		guardarTodo();
		mostrarAlquiler();
	}
}
// ********************************************************************************
// Metodo para editar un alquiler
// ********************************************************************************

function editAlq(index){
	alq_en_edicion = index;
	alq = datos.alquiler[index];
	for(key in alq){
		$('#'+key).val(alq[key]);
	}

}

function nuevoAlquiler() {
	window.location = window.location;
}

function buscarCliente() {
	$("#ocultalAlquiler").hide("slow");
	$("#mostrarAlq").show("slow");
}

function regresarAlquiler(){
	$("#ocultarCliente").show("slow");
	$("#mostrarAlq").hide("slow");
}
