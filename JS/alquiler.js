  
function enCarga(){
    filtrarCedula();
    filtrarMatricula();
    listarAlquiler();
    autoIncremento();
    $("#btnGuardarAlquiler").hide('slow');
    $("#btnCancelarAlquiler").hide('slow');
    $("#mostrarBuscarAlq").hide('slow');
    $("#datos").hide('slow');
}

function filtrarCedula(){
    var destino = document.getElementById('selectCedulaAl');
    db.collection("cliente").onSnapshot((querySnapshot) => {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <option value="${doc.data().cedula}" id="${doc.id}">${doc.data().cedula}</option>
            `
        });
    });
}

function filtrarCliente(cedulaCliente){  
    db.collection("cliente").where("cedula", "==", cedulaCliente)
    .onSnapshot(function(querySnapshot) {

        querySnapshot.forEach(function(doc) {
            $("#nombresAl").val(doc.data().nombres);
            $("#apellidosAl").val(doc.data().apellidos);
            $("#direccionAl").val(doc.data().direccion);
        });
    });
}

function filtrarMatricula(){
    var destino = document.getElementById('selectMatriculaAl');
    db.collection("vehiculo").onSnapshot((querySnapshot) => {
        destino.innerHTML = `<option value=""></option>`;
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <option value="${doc.data().matricula}" id="${doc.id}">${doc.data().matricula}</option>
            `
        });
    });
}

function autoOcupado(matricula){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 
    hoy = mm+'-'+dd+'-'+yyyy;
    db.collection("alquiler").where("matricula", "==", matricula).where("estado", "==", "OCUP")
    .onSnapshot(function(querySnapshot) {
        fEntrada = "";
        querySnapshot.forEach(function(doc) {
           fEntrada = doc.data().fecEntrada;
        });
        if(hoy < fEntrada){
            toastr.error("El vehiculo estara ocupado hasta: " + fEntrada,"Aviso!");
            //alert("El vehiculo estara ocupado hasta: " + fEntrada);
            $("#selectMatriculaAl").val("");
            $("#modeloAl").val("");
            $("#marcaAl").val("");
            $("#tarifaAl").val("");
        }
    });
}

function filtrarVehiculo(matriculaVehiculo){
    db.collection("vehiculo").where("matricula", "==", matriculaVehiculo)
    .onSnapshot(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            $("#marcaAl").val(doc.data().marca);
            $("#modeloAl").val(doc.data().modelo);
            $("#tarifaAl").val(doc.data().tarifa);
        });
    });
    autoOcupado(matriculaVehiculo);
}

function guardarAlquiler(){
    nAlquiler = $('#servicio').val();
    cedula = $('#selectCedulaAl').val();
    nombres = $('#nombresAl').val();
    apellidos = $('#apellidosAl').val();
    direccion = $('#direccionAl').val();
    matricula = $('#selectMatriculaAl').val();
    marca = $('#marcaAl').val();
    modelo = $('#modeloAl').val();
    tarifa = $('#tarifaAl').val();
    fecSalida = $('#salida').val();
    fecEntrada = $('#entrada').val();
    observacion = $('#observacion').val();
    estado = $('#estado').val();
    total = $('#pagar').val();
    
    db.collection("alquiler").add({
        "nAlquiler": nAlquiler,
		"cedula": cedula,
		"nombres": nombres,
		"apellidos": apellidos,
		"direccion": direccion,
		"matricula": matricula,
		"marca": marca,
		"modelo": modelo,
        "tarifa": tarifa,
        "fecSalida": fecSalida,
        "fecEntrada": fecEntrada,
        "observacion": observacion,
        "estado": estado,
        "total": total
    })
    .then(function(docRef) {
        limpiarAlquiler();
        bloquearCampos();
        ocultarBoton();
        autoIncremento();
        toastr.success("Datos Guardados","Exito");
    })
    .catch(function(error) {
        toastr.error("No se a podido Guardar","Aviso!");
    });
}

function nuevoAlquiler(){
    mostrarBoton();
    desbloquearCampos();
}

function buscarCliente(){
    $("#mostrarBuscarAlq").show('slow');
    $("#divBoton").hide('slow');
    $("#contenedorFormulario").hide('slow');
    $("#datos").show('slow');
}

function salirBuscar(){
    $("#mostrarBuscarAlq").hide('slow');
    $("#divBoton").show('slow');
    $("#contenedorFormulario").show('slow');
    $("#datos").hide('slow');
}

function cancelarVehiculo(){
    ocultarBoton();
    bloquearCampos();
    limpiarAlquiler();
    autoIncremento();
}

function mostrarBoton(){
    $("#btnCancelarAlquiler").show('slow');
    $("#btnGuardarAlquiler").show('slow');
    $("#btnNuevoAlquiler").hide('slow');
    $("#btnBuscarAlquiler").hide('slow');
}

function ocultarBoton(){
    $("#btnCancelarAlquiler").hide('slow');
    $("#btnGuardarAlquiler").hide('slow');
    $("#btnNuevoAlquiler").show('slow');
    $("#btnBuscarAlquiler").show('slow');
}

function buscarGeneralAlquiler(){
    var tableReg = document.getElementById('datos');
    var searchText = document.getElementById('searchTerm').value.toLowerCase();
    var cellsOfRow="";
    var found=false;
    var compareWith="";

    // Recorremos todas las filas con contenido de la tabla
    for (var i = 1; i < tableReg.rows.length; i++)
    {
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        found = false;
        // Recorremos todas las celdas
        for (var j = 0; j < cellsOfRow.length && !found; j++)
        {
            compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            // Buscamos el texto en el contenido de la celda
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
            {
                found = true;
            }
        }
        if(found)
        {
            tableReg.rows[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg.rows[i].style.display = 'none';
        }
    }
}

function listarAlquiler(){
    var destino = document.getElementById('tbDestino');
    db.collection("alquiler").orderBy("nAlquiler", "asc").onSnapshot((querySnapshot) => {
        destino.innerHTML = '';
        querySnapshot.forEach((doc) => {
            destino.innerHTML += `
            <tr>
                <td>${doc.data().nAlquiler}</td>
                <td>${doc.data().cedula}</td>
                <td>${doc.data().nombres} ${doc.data().apellidos}</td>
                <td>${doc.data().matricula}</td>
                <td><strong>${doc.data().marca}</strong> ${doc.data().modelo}</td>
                <td>${doc.data().tarifa}</td>
                <td>${doc.data().fecSalida}</td>
                <td>${doc.data().fecEntrada}</td>
                <td>${doc.data().observacion}</td>
                <td>${doc.data().estado}</td>
                <td>${doc.data().total}</td>
                <td><button class="btn btn-danger" onclick="borrarAlquiler('${doc.id}');"><i class="fas fa-trash"></i></button></td>
                <td><button class="btn btn-warning" 
                    onclick="actualizarAlquiler(
                        '${doc.id}','${doc.data().nAlquiler}',
                        '${doc.data().cedula}','${doc.data().nombres}',
                        '${doc.data().apellidos}','${doc.data().direccion}',
                        '${doc.data().matricula}','${doc.data().marca}',
                        '${doc.data().modelo}','${doc.data().tarifa}',
                        '${doc.data().fecSalida}','${doc.data().fecEntrada}',
                        '${doc.data().observacion}','${doc.data().estado}',
                        '${doc.data().total}'
                    )">
                        <i class="fas fa-edit"></i>
                    </button>
                </td>
            </tr>
            `
        });
    });
}

function borrarAlquiler(id){
    var respuesta = confirm("Seguro que desea Eliminar el Alquiler");
    if (respuesta == true) {
        db.collection("alquiler").doc(id).delete().then(function() {
            toastr.warning("Datos eliminado con éxito!","Aviso!");
        }).catch(function(error) {
            toastr.error("Problema al elminar los datos","Aviso!");
        });
    }else{
        toastr.warning("Datos no Eliminados","Aviso!");
    }
}

function actualizarAlquiler(id, nAlquiler, cedula, nombres, apellidos, direccion,
    matricula, marca, modelo, tarifa, fEntrada, fSalida, observacion, estado, total){
    $("#contenedorFormulario").show('slow');
    $("#datos").hide('slow');
    $("#mostrarBuscarAlq").hide('slow');
    $("#divBoton").show('slow');
    $("#estado").prop('disabled', false);
    mostrarBoton();

    $('#servicio').val(nAlquiler);
    $('#selectCedulaAl').val(cedula);
    $('#nombresAl').val(nombres);
    $('#apellidosAl').val(apellidos);
    $('#direccionAl').val(direccion);
    $('#selectMatriculaAl').val(matricula);
    $('#marcaAl').val(marca);
    $('#modeloAl').val(modelo);
    $('#tarifaAl').val(tarifa);
    $('#salida').val(fEntrada);
    $('#entrada').val(fSalida);
    $('#observacion').val(observacion);
    $('#estado').val(estado);
    $('#pagar').val(total);

    var btnModificar = document.getElementById('btnGuardarAlquiler');
    btnModificar.innerHTML = 'Modificar';
    btnModificar.onclick = () => {
        var respuesta = confirm("Seguro que desea Modificar el estado del Alquiler # " + nAlquiler);
        if (respuesta == true) {
            estadoM = $('#estado').val();
            var clienteRef = db.collection("alquiler").doc(id);
            return clienteRef.update({
                "estado": estadoM
            })
            .then(function() {
                toastr.success("Datos actualidados","Exito!");
                cancelarVehiculo();                
            })
            .catch(function(error) {
                toastr.warning("Error de la base de datos","Aviso!");
                cancelarVehiculo();  
            });
        }else{
            toastr.warning("Datos no actualidados","Aviso!");
            cancelarVehiculo();
        }
    }
}

function desbloquearCampos(){
    $("#selectCedulaAl").prop('disabled', false);
    $("#selectMatriculaAl").prop('disabled', false);
    $("#salida").prop('disabled', false);
    $("#entrada").prop('disabled', false);
    $("#observacion").prop('disabled', false);
    $("#estado").prop('disabled', false);
}

function bloquearCampos(){
    $("#selectCedulaAl").prop('disabled', true);
    $("#selectMatriculaAl").prop('disabled', true);
    $("#salida").prop('disabled', true);
    $("#entrada").prop('disabled', true);
    $("#observacion").prop('disabled', true);
    $("#estado").prop('disabled', true);
}

function limpiarAlquiler(){
    $("#servicio").val("");
    $("#selectCedulaAl").val("");
    $("#apellidosAl").val("");
    $("#nombresAl").val("");
    $("#direccionAl").val("");
    $("#selectMatriculaAl").val("");
    $("#modeloAl").val("");
    $("#marcaAl").val("");
    $("#tarifaAl").val("");
    $("#salida").val("");
    $("#observacion").val("");
    $("#pagar").val("");
    $("#entrada").val("");
    $("#estado").val("");   
}

function calcularDias(){
    var fecha_s = document.getElementById("salida").value;
    var fecha_e = document.getElementById("entrada").value;
  
        var fecha1 = new Date(fecha_s);
        var fecha2 = new Date(fecha_e);
  
            var diasDif = fecha2.getTime() - fecha1.getTime();
        var dias = Math.round(diasDif/(1000 * 60 * 60 * 24));
  
      var tarifaAl = document.getElementById("tarifaAl").value;
  
      var totalPagar = tarifaAl * dias;
  
      document.getElementById("pagar").value = totalPagar;
  
}

function autoIncremento(){
    db.collection("alquiler").onSnapshot((querySnapshot) => {
        $("#servicio").val("");
        contador = 0;
        querySnapshot.forEach((doc) => {
            contador++;
        });
        $("#servicio").val(contador + 1);
    });
}

function validarFecha(fEntrada){
    fSalida = $("#salida").val();

    if(fSalida > fEntrada){
        toastr.warning("Porfavor ingrese una fecha mayor a la Fecha de Salida: " + fSalida,"Aviso!");
        $("#entrada").val("");
    }
}

function validarGuardado(){
    var cedula = $("#selectCedulaAl").val();
    var matricula = $("#selectMatriculaAl").val();
    var salida = $("#salida").val();
    var entrada = $("#entrada").val();
    var estado = $("#estado").val();

    if($.trim(cedula) == ""){
        toastr.error("Seleccione a un cliente","Aviso!");
        $("#selectCedulaAl").addClass("error");
    }else if($.trim(matricula) == ""){
        $("#selectCedulaAl").removeClass("error");
        toastr.error("Seleccione a una Matricula","Aviso!");
        $("#selectMatriculaAl").addClass("error");
    }else if($.trim(salida) == ""){
        $("#selectMatriculaAl").removeClass("error");
        toastr.error("Seleccione la Fecha de Salida","Aviso!");
        $("#salida").addClass("error");    
    }else if($.trim(entrada) == ""){
        $("#salida").removeClass("error");
        toastr.error("Seleccione la Fecha de Entrada","Aviso!");
        $("#entrada").addClass("error"); 
    }else if($.trim(estado) == ""){
        $("#entrada").removeClass("error");
        toastr.error("Seleccione el estado del Vehiculo","Aviso!");
        $("#estado").addClass("error"); 
    }else{
        $("#estado").removeClass("error");
        guardarAlquiler();
    }
}


