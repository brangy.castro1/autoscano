
  // Funcion para cuando recarga la pagina se muestre la lista y oculte varios botones
function enCarga(){
    listarCliente();
    $("#btnGuardar").hide('slow');
    $("#btnCancelar").hide('slow');
    $("#formBuscar").hide('slow');
    $("#datos").hide('slow');
  }

//Funcion para guardar los clientes

  function guardarCliente(){
        cedula = $('#cedula').val();
        nombres = $('#nombres').val();
        apellidos = $('#apellidos').val();
        ciudad = $('#ciudad').val();
        direccion = $('#direccion').val();
        telefono1 = $('#telefono1').val();
        telefono2 = $('#telefono2').val();
        genero = $('#genero').val();
        fecha = $('#fecha').val();
        correo = $('#correo').val();

    db.collection("cliente").add({
        cedula: cedula,
        nombres: nombres,
        apellidos: apellidos,
        ciudad: ciudad,
        direccion: direccion,
        telefono1: telefono1,
        telefono2: telefono2,
        genero: genero,
        fecha: fecha,
        correo: correo
    })
    .then(function(docRef) {
        botonCancelar();
        toastr.success("Datos Guardados","Exito");
    })
    .catch(function(error) {
        toastr.error("No se a podido Guardar","Aviso!");
    });
  }

//Funcion para listar todos los clientes en una tabla

  function listarCliente(){
    var tbCliente = document.getElementById('tbCliente');
    db.collection("cliente").onSnapshot((querySnapshot) => {
        tbCliente.innerHTML = '';
        querySnapshot.forEach((doc) => {
            tbCliente.innerHTML += `
            <tr>
                <td>${doc.data().cedula}</td>
                <td>${doc.data().nombres} ${doc.data().apellidos}</td>
                <td>${doc.data().ciudad}</td>
                <td>${doc.data().direccion}</td>
                <td>${doc.data().telefono1}</td>
                <td>${doc.data().genero}</td>
                <td>${doc.data().fecha}</td>
                <td>${doc.data().correo}</td>
                <td><button class="btn btn-danger" onclick="borrarCliente('${doc.id}');"><i class="fas fa-trash"></i></button></td>
                <td><button class="btn btn-warning" 
                    onclick="actualizarCliente(
                        '${doc.id}','${doc.data().cedula}',
                        '${doc.data().nombres}','${doc.data().apellidos}',
                        '${doc.data().ciudad}','${doc.data().direccion}',
                        '${doc.data().telefono1}','${doc.data().telefono2}',
                        '${doc.data().genero}','${doc.data().fecha}',
                        '${doc.data().correo}'
                        )">
                        <i class="fas fa-edit"></i>
                    </button>
                </td>
            </tr>
            `
        });
    });
  }

  // Funcion para Eliminar un Cliente

  function borrarCliente(id){
    db.collection("cliente").doc(id).delete().then(function() {
        console.log("Document successfully deleted!");
    }).catch(function(error) {
        console.error("Error removing document: ", error);
    });
  }

  // Funcion para Actualizar los Cliente

  function actualizarCliente(id,cedula,nombres,apellidos,ciudad,direccion,telefono1,telefono2,genero,fecha,correo){
    $("#contenedorFormulario").show('slow');
    $("#divBoton").show('slow');
    $("#datos").hide('slow');
    $("#formBuscar").hide('slow');
    mostrarBoton();
    desbloquearCampos();

    $('#cedula').val(cedula);
    $('#nombres').val(nombres);
    $('#apellidos').val(apellidos);
    $('#ciudad').val(ciudad);
    $('#direccion').val(direccion);
    $('#telefono1').val(telefono1);
    $('#telefono2').val(telefono2);
    $('#genero').val(genero);
    $('#fecha').val(fecha);
    $('#correo').val(correo);
    

    var btnModificar = document.getElementById('btnGuardar');
    btnModificar.innerHTML = 'Modificar';
    btnModificar.onclick = function(){
        var respuesta = confirm("Seguro que desea modificar los datos de: " + nombres + " " + apellidos);

        if (respuesta == true) {
            cedula = $('#cedula').val();
            nombres = $('#nombres').val();
            apellidos = $('#apellidos').val();
            ciudad = $('#ciudad').val();
            direccion = $('#direccion').val();
            telefono1 = $('#telefono1').val();
            telefono2 = $('#telefono2').val();
            genero = $('#genero').val();
            fecha = $('#fecha').val();
            correo = $('#correo').val();

            var clienteRef = db.collection("cliente").doc(id);

            // Set the "capital" field of the city 'DC'
            return clienteRef.update({
                cedula: cedula,
                nombres: nombres,
                apellidos: apellidos,
                ciudad: ciudad,
                direccion: direccion,
                telefono1: telefono1,
                telefono2: telefono2,
                genero: genero,
                fecha: fecha,
                correo: correo
            })
            .then(function() {
                toastr.success("Datos actualidados","Exito!");
                botonCancelar();
            })
            .catch(function(error) {
                toastr.warning("Datos no actualidados","Aviso!");
            });
          } else {
            toastr.warning("Datos no actualidados","Aviso!");
            botonCancelar();
          }        
    }  
  }

  // funcion para limpiar los campos
function limpiarCliente(){
    $('#cedula').val("");
    $('#nombres').val("");
    $('#apellidos').val("");
    $('#ciudad').val("");
    $('#direccion').val("");
    $('#telefono1').val("");
    $('#telefono2').val("");
    $('#genero').val("");
    $('#fecha').val("");
    $('#correo').val("");
    $('#edad').val("");
}

  // Funcion para el Boton Buscar
function botonBuscar(){
    $("#datos").show('slow');
    $("#formBuscar").show('slow');
    $("#divBoton").hide('slow');
    $("#contenedorFormulario").hide('slow');
}

  // Funcion para el Boton Regresar
function botonRegresar(){
    $("#datos").hide('slow');
    $("#formBuscar").hide('slow');
    $("#divBoton").show('slow');
    $("#contenedorFormulario").show('slow');
}

// Funcion para el Boton Nuevo
function botonNuevo(){
    desbloquearCampos();
    mostrarBoton();
}

function bloquearCampos(){
    $('#cedula').prop('disabled', true);
    $('#nombres').prop('disabled', true);
    $('#apellidos').prop('disabled', true);
    $('#ciudad').prop('disabled', true);
    $('#direccion').prop('disabled', true);
    $('#telefono1').prop('disabled', true);
    $('#telefono2').prop('disabled', true);
    $('#genero').prop('disabled', true);
    $('#fecha').prop('disabled', true);
    $('#correo').prop('disabled', true);
}

function desbloquearCampos(){
    $('#cedula').prop('disabled', false);
    $('#nombres').prop('disabled', false);
    $('#apellidos').prop('disabled', false);
    $('#ciudad').prop('disabled', false);
    $('#direccion').prop('disabled', false);
    $('#telefono1').prop('disabled', false);
    $('#telefono2').prop('disabled', false);
    $('#genero').prop('disabled', false);
    $('#fecha').prop('disabled', false);
    $('#correo').prop('disabled', false);
}

function mostrarBoton(){
    $("#btnCancelar").show('slow');
    $("#btnGuardar").show('slow');
    $("#btnNuevo").hide('slow');
    $("#btnBuscar").hide('slow');
}

function ocultarBoton(){
    $("#btnCancelar").hide('slow');
    $("#btnGuardar").hide('slow');
    $("#btnNuevo").show('slow');
    $("#btnBuscar").show('slow');
}

function botonCancelar(){
    ocultarBoton();
    bloquearCampos();
    limpiarCliente();
}

function buscarCliente(){

        var tableReg = document.getElementById('datos');
        var searchText = document.getElementById('searchTerm').value.toLowerCase();
        var cellsOfRow="";
        var found=false;
        var compareWith="";

        // Recorremos todas las filas con contenido de la tabla
        for (var i = 1; i < tableReg.rows.length; i++)
        {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;
            // Recorremos todas las celdas
            for (var j = 0; j < cellsOfRow.length && !found; j++)
            {
                compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
                {
                    found = true;
                }
            }
            if(found)
            {
                tableReg.rows[i].style.display = '';
            } else {
                // si no ha encontrado ninguna coincidencia, esconde la
                // fila de la tabla
                tableReg.rows[i].style.display = 'none';
            }
        }
}

function validarCedula() {
    var cad = document.getElementById("cedula").value.trim();
    var total = 0;
    var longitud = cad.length;
    var longcheck = longitud - 1;

    if (cad !== "" && longitud === 10){
      for(i = 0; i < longcheck; i++){
        if (i%2 === 0) {
          var aux = cad.charAt(i) * 2;
          if (aux > 9) aux -= 9;
          total += aux;
        } else {
          total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
        }
      }

      total = total % 10 ? 10 - total % 10 : 0;

      if (cad.charAt(longitud-1) == total) {
        $("#cedula").removeClass("error");
      }else{
        toastr.error("Porfavor ingrese una cedula validad","Aviso!");
        $("#cedula").addClass("error");
        $("#cedula").val("");
      }
    }
}

function validateMail(idMail){
	// Patron para el correo
	var patron=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
	if(!idMail.search(patron)==0){
        //Mail incorrecto
        toastr.error("Porfavor ingrese un Correo validad","Aviso!");
        $("#correo").addClass("error");
        $("#correo").val("");
	}
}

function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57)
}

function isValidDate(day,month,year){
    var dteDate;
  
    month=month-1;
    dteDate = new Date(year,month,day);
  
    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
  }
  
  function validate_fecha(fecha) {
  
    var patron = new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");
  
    if(fecha.search(patron)==0){
      var values = fecha.split("-");
      if(isValidDate(values[2],values[1],values[0])){
        return true;
      }
    }
    return false;
  }

function calcularEdad(){
    var fecha = document.getElementById("fecha").value;
    if(validate_fecha(fecha)==true){
      var values = fecha.split("-");
      var dia = values[2];
      var mes = values[1];
      var ano = values[0];
  
      var fecha_hoy = new Date();
      var ahora_ano = fecha_hoy.getYear();
      var ahora_mes = fecha_hoy.getMonth();
      var ahora_dia = fecha_hoy.getDate();
  
      var edad = (ahora_ano + 1900) - ano;
      if(ahora_mes < mes){
        edad--;
      }
      if((mes == ahora_mes) && (ahora_dia < dia)){
        edad--;
      }
      if(edad > 1900){
        edad -= 1900;
      }
  
      var meses = 0;
      if(ahora_mes > mes){
        meses = ahora_mes - mes;
      }
      if(ahora_mes < mes){
        meses = 12 - (mes - ahora_mes);
      }
      if(ahora_mes == mes && dia > ahora_dia){
        meses = 11;
      }
  
      var dias = 0;
    if(ahora_dia > dia){
        dias = ahora_dia - dia;
      }
    if(ahora_dia < dia){
        ultimoDiaMes = new Date(ahora_ano, ahora_mes, 0);
        dias = ultimoDiaMes.getDate()-(dia - ahora_dia);
      }
      document.getElementById("edad").value = edad;
      $("#fecha").removeClass("error");
    }else{
        toastr.error("Porfavor ingrese una Fecha Nacimiento validad","Aviso!");
         $("#fecha").addClass("error");
    }
}

function guardarClienteValidado(){

    cedula = $('#cedula').val();
    nombres = $('#nombres').val();
    apellidos = $('#apellidos').val();
    ciudad = $('#ciudad').val();
    direccion = $('#direccion').val();
    telefono1 = $('#telefono1').val();
    telefono2 = $('#telefono2').val();
    genero = $('#genero').val();
    fecha = $('#fecha').val();
    correo = $('#correo').val();
  
    if(cedula == ""){
      $("#cedula").addClass("error");
      toastr.error("Ingrese Cedula","Aviso!");
    }else if(nombres == ""){
      $("#cedula").removeClass("error");
      toastr.error("Ingrese Nombres","Aviso!");
      $("#nombres").addClass("error");
    }else if(apellidos == ""){
      $("#nombres").removeClass("error");
      toastr.error("Ingrese Apellidos","Aviso!");
      $("#apellidos").addClass("error");
    }else if(ciudad == ""){
      $("#apellidos").removeClass("error");
      toastr.error("Ingrese Ciudad","Aviso!");
      $("#ciudad").addClass("error");
    }else if(direccion == ""){
      $("#ciudad").removeClass("error");
      toastr.error("Ingrese Direccion","Aviso!");
      $("#direccion").addClass("error");
    }else if(telefono1 == ""){
      $("#direccion").removeClass("error");
      toastr.error("Ingrese Telefono 1","Aviso!");
      $("#telefono1").addClass("error");
    }else if(telefono2 == ""){
      $("#telefono1").removeClass("error");
      toastr.error("Ingrese Telefono 2","Aviso!");
      $("#telefono2").addClass("error");
    }else if(genero == ""){
      $("#telefono2").removeClass("error");
      toastr.error("Ingrese Genero","Aviso!");
      $("#genero").addClass("error");
    }else if(correo == ""){
      $("#genero").removeClass("error");
      toastr.error("Ingrese Correo","Aviso!");
      $("#correo").addClass("error");
    }else if(fecha == ""){
      $("#correo").removeClass("error");
      toastr.error("Ingrese Fecha de nacimiento","Aviso!");
      $("#fehca").addClass("error");
    }else{
      $("#fecha").removeClass("error");
      guardarCliente();
    }
  
  
  }